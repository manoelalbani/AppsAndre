class DeclaracaoVariaveis {
	public static void main (String args[]){
		String nomePessoa = new String("Andre");
		int idadePessoa=21;
		String rgPessoa = new String("3944577");
		char sexoPessoa='M';
		Double salarioPessoa=1000D;
		System.out.println("Senhor "+nomePessoa+", de "+idadePessoa+" anos, portador do RG de numero "+rgPessoa+", do sexo "+sexoPessoa+", esta registrado com salario de R$ "+salarioPessoa);
	}
}