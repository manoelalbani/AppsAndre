class ExemploArray{
	public static void main(String args[]){
		//array
		int x[]={1,4,3,6,2,7};
		//percorrer array
		for (int i=0; i<x.length;i++){
			System.out.println("Valor de i "+x[i]);
		}
		//array multidimensional
		int y[][]={{1,3,4},{1}, {1,5,3,2}};
		//percorrer array multi
		for (int i=0;i<y.length;i++){
			for (int j=0;j<y[i].length;j++){
				System.out.println("Valor de i "+i+" valor de j "+j+" valor no array"+y[i][j]);
			}
		}
		//aumentar array
		int temp[]=x;
		x=new int[x.length+1];
		for (int i=0; i<temp.length;i++){
			x[i]=temp[i];
		}
		//ateh aqui
		for (int i=0; i<x.length;i++){
			System.out.println("novo Valor de i "+x[i]);
		}

		//enhanced for
		//array normal
		for(int elemento : x){
			System.out.println("percorrer o array x com foreach");
		}
		//array multidimensional
		int i=0;
		int j=0;
		for (int[] linha :y){
			j=0;
			for(int coluna : linha){
				System.out.println("Valor de i "+i+" valor de j "+j+" valor no array"+y[i][j]);			
				j++;
			}
			i++;		
		}

	}
}