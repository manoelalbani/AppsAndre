import java.io.File;
import java.io.IOException;
public class CriadorArquivos{
	//utilizando o throws pra que caso de erro na criacao do arquivo, para que quem tiver chamando esse metodo tenha que trate essa exceção
	public void criarArquvi(String caminho) throws IOException{
		File f = new File(caminho);
		f.createNewFile();
	}
}