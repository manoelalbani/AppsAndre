public class Funcionario extends Pessoa{
	private String ctps;

	public Funcionario(String nome, String cpf, String ctps ){
		//criando a classe mae
		super(nome, cpf);
		this.ctps=ctps;
	}

	public void setCtps(String ctps){
		this.ctps=ctps;
	}

	public String getCtps(){
		return ctps;
	}

	//@overwrite / sobrescrita
	public void imprimeDados(){
		super.imprimeDados();	
		System.out.println("Ctps: "+getCtps());
	}

	//@overwrite / sobrescrita
	public String toString(){
		return getNome();
	}

}