package br.com.voffice;

import br.com.voffice.rh.Contabilidade;
import br.com.voffice.dados.*;
public class TestaContabilidade {
	public static void main(String[] args) {
		Funcionario fclt1 = new FuncionarioClt("Maria", "22222", 1000);
		Funcionario fclt2 = new FuncionarioClt("Jose", "33333", 2000);
		Funcionario fclt3 = new FuncionarioClt("Joaquim", "33333", 3000);

		Funcionario fpj1 = new FuncionarioPj("Amanda", "44444", 150, 10);
		Funcionario fpj2 = new FuncionarioPj("Marcelo", "555555", 100, 10);
		Funcionario fpj3 = new FuncionarioPj("Joao", "66666", 50, 10);

		Estagiario e1 = new Estagiario("Lucas", "", 2000, 20);


		Funcionario[] funcionarios = {fclt1, fclt2, fclt3, fpj1, fpj2, fpj3, e1};

		Contabilidade contabilidade = new Contabilidade();

		double valorFolha = contabilidade.calcularFolhaPagamento(funcionarios);

		System.out.println("Valor da folha de pagamento: " + valorFolha);
	}
}