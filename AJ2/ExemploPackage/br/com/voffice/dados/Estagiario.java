package br.com.voffice.dados;

public class Estagiario extends Pessoa implements Funcionario {
	public double salarioBase;
	public double percentualBolsa;

	public Estagiario(String nome, String cpf, double salarioBase, double percentualBolsa) {
		super(nome, cpf);
		this.salarioBase = salarioBase;
		this.percentualBolsa = percentualBolsa;
	}

	public double calculaSalario() {
		return salarioBase * percentualBolsa / 100;
	}

	//metodo get;

	//metodo set;
}