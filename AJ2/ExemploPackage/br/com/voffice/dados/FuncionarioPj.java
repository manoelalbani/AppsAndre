package br.com.voffice.dados;

public class FuncionarioPj extends Pessoa implements Funcionario {
	public double valorHora;
	public int quantidadeHoras;

	public FuncionarioPj(String nome, String cpf, double valorHora, int quantidadeHoras) {
		super(nome, cpf);
		this.valorHora = valorHora;
		this.quantidadeHoras = quantidadeHoras;
	}

	public double calculaSalario() {
		return valorHora * quantidadeHoras;
	}

	public double getValorHora() {
		return valorHora;
	}

	public void setValorHora(double valorHora) {
		this.valorHora = valorHora;
	}

	public int getQuantidadeHoras() {
		return quantidadeHoras;
	}

	public void setQuantidadeHoras(int quantidadeHoras) {
		this.quantidadeHoras = quantidadeHoras;
	}

	
}