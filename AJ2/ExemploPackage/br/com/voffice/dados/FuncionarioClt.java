package br.com.voffice.dados;

public class FuncionarioClt extends Pessoa implements Funcionario {
	public double salario;

	public FuncionarioClt(String nome, String cpf, double salario) {
		super(nome, cpf);
		this.salario = salario;
	}

	public double calculaSalario() {
		return salario;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
}