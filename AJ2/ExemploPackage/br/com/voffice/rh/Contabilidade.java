package br.com.voffice.rh;

import br.com.voffice.dados.Funcionario;

public class Contabilidade {
	public double calcularFolhaPagamento(Funcionario[] funcionarios) {
		double valorFolha = 0;
		for (Funcionario f : funcionarios) {
			valorFolha += f.calculaSalario();
		}
		return valorFolha;
	}
}