/*
 * Globalcode - "The Developers Company"
 * 
 * Academia do Java
 */
public class TestaConta {

    public static void main(String[] args) {
        // Criar um objeto do tipo Conta
        Conta conta = new Conta();
        // Usar o metodo inicializaConta para fazer a inicializacao do objeto criado
        conta.inicializaConta(1500, "231690", "Andre", 18082, 1);
        // executar um deposito
        conta.deposito(1000);
        // Imprimir o saldo apos o deposito
        conta.imprimeDados();
        // executar um saque cujo valor seja menor que o saldo
        conta.saque(900);
        // Imprimir o saldo apos o deposito
        conta.imprimeDados();
        // executar uma retirada cujo valor seja maior que o saldo
        conta.saque(2000);
        // Imprimir o saldo apos o deposito
        conta.imprimeDados();
    }
}
