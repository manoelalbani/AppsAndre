public class Funcionario extends Pessoa{
	private double salario;

	public Funcionario(){

	}

	public Funcionario(String nome, String cpf, double salario){
		super(nome, cpf);
		this.salario=salario;
	}

	public void imprimeDados(){
		super.imprimeDados();
		System.out.println("Salario: "+salario);
	}
}