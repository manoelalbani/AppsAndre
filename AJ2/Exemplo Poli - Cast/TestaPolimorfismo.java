public class TestaPolimorfismo{
 public static void main(String[] args) {
		Pessoa p = new Pessoa("Maria", "1111111");
		Pessoa f  = new Funcionario("Joao","01020120", 1020);

		p.imprimeDados();
		System.out.println("=====================");
		f.imprimeDados();
	}
}