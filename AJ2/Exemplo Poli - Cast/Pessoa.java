public  class Pessoa implements Transportavel{
	
	private String nome;
	private String cpf;

	public Pessoa(){

	}

	public Pessoa(String  nome, String cpf){
		this.nome=nome;
		this.cpf=cpf;
	}

	public double getPeso(){
		return 130;
	}
	public double getVolume(){
		return 110;
	}
	public int getEmpilhamentoMax(){
		return 1;
	}

	public void imprimeDados(){
		System.out.println("Nome: "+nome);
		System.out.println("CPF: "+cpf);

	}
}