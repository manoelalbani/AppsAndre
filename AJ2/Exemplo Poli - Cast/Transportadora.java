public class Transportadora{
	public static void main(String[] args){
		Transportadora t = new Transportadora();
		
		//Transportavel - exemplo de polimorfismo, pq o mob é um transportavel porem recebe uma instacia de mobilia.
		Transportavel mob = new Mobilia();
		System.out.println("=============Transportando Mobilia=============");
		t.transportar(mob);
		Transportavel ali = new Alimento();
		System.out.println("=============Transportando Alimento============");
		t.transportar(ali);

		//Pessoa p = new Pessoa();
		//System.out.println("=============Transportando Pessoa==============");
		//t.transportar(p);
		
		Transportavel f = new Funcionario();
		System.out.println("=============Transportando Funcionario=========");
		t.transportar(f);

	}
	public void transportar(Transportavel coisa){
		System.out.println("Instacia de Pessoa? "+(coisa instanceof Pessoa));
		System.out.println("Posso empilhar ate "+ coisa.getEmpilhamentoMax());

	}
}