public class Transportadora{
	public static void main(String[] args){
		Transportadora t = new Transportadora();
		Mobilia mob = new Mobilia();
		System.out.println("=============Transportando Mobilia=============");
		t.transportar(mob);
		Alimento ali = new Alimento();
		System.out.println("=============Transportando Alimento============");
		t.transportar(ali);

		//Pessoa p = new Pessoa();
		//System.out.println("=============Transportando Pessoa==============");
		//t.transportar(p);
		
		Funcionario f = new Funcionario();
		System.out.println("=============Transportando Funcionario=========");
		t.transportar(f);

	}
	public void transportar(Transportavel coisa){
		System.out.println("Posso empilhar ate "+ coisa.getEmpilhamentoMax());

	}
}