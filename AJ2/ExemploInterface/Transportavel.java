public interface Transportavel{
	double getPeso();
	double getVolume();
	int getEmpilhamentoMax();
}