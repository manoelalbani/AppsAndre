bh public class Produto {
	private int codigo;
	private double preco;
	private String nome;
	private String nomeFabricante;

	public int getCodigo(){
		return codigo;
	}

	public void setCodigo(int novoCodigo){
		codigo=novoCodigo;
	}

	public double getPreco(){
		return preco;
	}

	public void setPreco(double novoPreco){
		preco=novoPreco;
	}
	public String getNome(){
		return nome;
	}

	public void setNome(int novoNome){
		nome=novoNome;
	}
	public String getnNomeFabricante(){
		return nomeFabricante;
	}

	public void setNomeFabricante(String novoNomeFabricante){
		nomeFabricante=novoNomeFabricante;
	}

}