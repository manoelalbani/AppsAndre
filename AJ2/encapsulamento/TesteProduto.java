class TesteProduto {
	public static void main(String args[]) {
		Produto produto1 = new Produto();
		produto1.codigo = 1;
		produto1.preco = 200;
		produto1.nome = "Radio de pilha";
		produto1.nomeFabricante = "Motoradio";

		System.out.println("Imprimindo Produto 1");
		System.out.println("codigo: " + produto1.codigo);
		System.out.println("preco: " + produto1.preco);
		System.out.println("nome: " + produto1.nome);
		System.out.println("nomeFabricante: " + produto1.nomeFabricante);
		System.out.println("");

		produto1.aumentarPreco(0.1);

		System.out.println("preco: " + produto1.preco);

		Produto produto2 = new Produto();
		produto2.codigo = 2;
		produto2.preco = 2000;
		produto2.nome = "TV";
		produto2.nomeFabricante = "LG";

		System.out.println("Imprimindo Produto 2");
		System.out.println("codigo: " + produto2.codigo);
		System.out.println("preco: " + produto2.preco);
		System.out.println("nome: " + produto2.nome);
		System.out.println("nomeFabricante: " + produto2.nomeFabricante);
		System.out.println("");

		new Produto();
		Produto produto3 = new Produto();

		System.out.println("Imprimindo Produto 3");
		System.out.println("codigo: " + produto3.codigo);
		System.out.println("preco: " + produto3.preco);
		System.out.println("nome: " + produto3.nome);
		System.out.println("nomeFabricante: " + produto3.nomeFabricante);
		System.out.println("cor: " + produto3.cor);
		System.out.println("");

		String arr[] = {"a", "batata", "c"};

		produto1.testeArray(arr);
		produto1.testeVarArg(1, "a", "b");
	}
}