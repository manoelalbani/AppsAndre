public class Departamento{
	private String nome;
	private Pessoa[] pessoas;

	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return nome;
	}
	public void setPessoas(Pessoa[] pessoas){
		this.pessoas=pessoas;
	}
	public Pessoa[] getPessoas(){
		return pessoas;
	}
	public void addPessoa(Pessoa p){
		int tamanho=pessoas ==null ? 0 :pessoas.length;
		Pessoa[] tmep = new Pessoa[tamanho+1];
		for (int i=0;i<tamanho;i++){
			temp[i]=pessoas[i];
		} 
		temp[tamanho]=p;
		pessoas=temp;
	}
	public void imprimeDados(){
		System.out.println("=======================");
		System.out.println("Dados do Departamento: ");
		System.out.println("Nome: "+nome);
		System.out.println("=======================");
		System.out.println("Funcionarios");
		for(Pessoa p:pessoas){
			System.out.println("Nome: "+p.getNome());
			System.out.println("CPF: "+p.getCpf());
			System.out.println("=======================");
		}
		
		
	}
}