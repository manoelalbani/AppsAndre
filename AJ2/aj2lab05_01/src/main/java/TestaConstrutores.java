/*
 * Globalcode - "The Developers Company"
 * 
 * Academia do Java 
 * 1) Construa um objeto da classe Conta com cada construtor e chame o metodo imprimeDados 
 * 2) Construa um objeto da classe Cliente e chame o metodo imprimeDados
 * 
 */
public class TestaConstrutores {

    public static void main(String[] args) {
    	Conta c1 = new Conta(1200, "1234", "Andre", "18082", 1);
    	c1.imprimeDados();
    	Conta c2 = new Conta("1234", "Andre", "18082", 1);
    	c2.imprimeDados();
    	Cliente cliente = new Cliente("Andre", "004343181881");
    	cliente.imprimeDados();

    }
}
