
/*
 * Globalcode - "The Developers Company"
 * 
 * Academia do Java
 */
class TestaConta {

    public static void main(String[] args) {
        // Criacao da conta
        Conta conta = new Conta();
        // Inicializacao da conta
        conta.inicializaConta(1000, "23169", "Andre", 18082, 1);
        // Impressao dos dados da conta
        conta.imprimeDados();
        // Saque da conta
        conta.saque(5);
        // Impressao dos dados da conta
        conta.imprimeDados();
        // Deposito em conta
        conta.deposito(500);
        // Impressao dos dados da conta
        conta.imprimeDados();
    }
}
