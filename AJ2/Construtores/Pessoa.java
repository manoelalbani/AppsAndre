public class Pessoa{
	

	//variaveis
	
	private String nome;
	private String cpf;
	private String dataNascimento;
	
	//inicializacao de objetos
	{
		System.out.println("Inicializacao de objetos");
	}

	//Construtor

	public Pessoa(String nome, String cpf){
		this.setNome(nome);
		this.setCpf(cpf);
	}

	//sobrecarga de constutor
	public Pessoa(){

	}
	public Pessoa(String nome, String cpf, String dataNascimento){
		this(nome, cpf);
		this.setDataNascimento(dataNascimento);
	}

	//getters and setters
	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return nome;
	}
	public void setCpf(String cpf){
		this.cpf=cpf;
	}
	public String getCpf(){
		return cpf;
	}
	public void setDataNascimento(String dataNascimento){
		this.dataNascimento=dataNascimento;
	}
	public String getDataNascimento(){
		return dataNascimento;
	}

	//imprimir dados na tela
	public void imprimeDados(){
		System.out.println("Nome: "+ getNome());
		System.out.println("Cpf: "+ getCpf());
	}
}