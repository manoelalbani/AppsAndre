public class SaldoInsuficienteException extends Exception{
	
	public SaldoInsuficienteException(String erro){
		super(erro);
	}
}