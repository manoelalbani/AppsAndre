/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio2;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author manoa
 */
@Named
@RequestScoped
public class ItemProcessor {
    @Inject
    private DefaultItemDao itemDao;
    public void execute(){
        List<Item> items = itemDao.fetchItems();
        for (Item i: items){
            System.out.println("Found Item: "+i);
        }
    }
   
    
}
