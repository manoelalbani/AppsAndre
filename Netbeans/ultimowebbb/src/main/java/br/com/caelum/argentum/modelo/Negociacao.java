package br.com.caelum.argentum.modelo;
import java.util.Calendar;

public final class Negociacao {
	private double preco;
	private int quantidade;
	private Calendar data;
	public Negociacao(double preco, int quantidade, Calendar data) {
		this.preco = preco;
		this.quantidade = quantidade;
		this.data = data;
	}
	public double getPreco() {
		return preco;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public Calendar getData() {
		return data;
	}
	public double getVolume() {
		return preco * quantidade;
		}

	

}
