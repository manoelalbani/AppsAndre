/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemploio.serializable;

import br.com.voffice.exemploio.model.Pessoa;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author manoa
 */
public class ExemploDesserializador  {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream fl = new FileInputStream("pessoa.dat");
        ObjectInputStream ob = new ObjectInputStream(fl);
        Pessoa p = (Pessoa) ob.readObject();
        System.out.println(p);
        
    }
}
