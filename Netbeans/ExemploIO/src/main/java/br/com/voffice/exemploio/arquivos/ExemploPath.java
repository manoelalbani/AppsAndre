/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemploio.arquivos;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author manoa
 */
public class ExemploPath {
    public static void main(String[] args) {
        Path var= Paths.get("/var");
        System.out.println(var);
        System.out.println(var.toUri());
        System.out.println("Permite Leitura: "+Files.isReadable(var));
        System.out.println("Permite Escrita: "+Files.isWritable(var));
       
    }
}
