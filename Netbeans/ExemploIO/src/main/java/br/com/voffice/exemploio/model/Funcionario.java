/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemploio.model;

/**
 *
 * @author manoa
 */
public class Funcionario extends Pessoa{
    private String carteiraTrabalho;
    public Funcionario(String carteiraTrabalho, String nome, String cpf) {
            super(nome, cpf);
            this.carteiraTrabalho = carteiraTrabalho;
        }

    public Funcionario() {
    }
    
    
    
    public String getCarteiraTrabalho() {
        return carteiraTrabalho;
    }

    public void setCarteiraTrabalho(String carteiraTrabalho) {
        this.carteiraTrabalho = carteiraTrabalho;
    }

   
    
}
