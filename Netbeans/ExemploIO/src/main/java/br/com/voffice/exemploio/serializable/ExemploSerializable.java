/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemploio.serializable;

import br.com.voffice.exemploio.model.Funcionario;
import br.com.voffice.exemploio.model.Pessoa;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author manoa
 */
public class ExemploSerializable {
    public static void main(String[] args) throws IOException {
        Pessoa p = new Pessoa("Maria", "1111");
        
        Funcionario f = new Funcionario("222", "jorge", "222");
        
        FileOutputStream fos = new FileOutputStream("pessoa.dat");
        
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(f);
    }
}
