
package br.com.voffice.exemplothreads;

public class RunnableExemplo implements Runnable{

    int numero;
    char c;

    public RunnableExemplo(int numero, char c) {
        this.numero = numero;
        this.c = c;
        System.out.printf("Thread Vai imprimir %d vezes o caractere %c %n", this.numero, this.c);
    }
    
    @Override
    public void run(){
        for(int i=0; i<this.numero;i++){
            System.out.print(c);
        }
    }
}
