
package br.com.voffice.exemplothreads;

public class TesteThreadExemplo {
    public static void main(String[] args) {
        ThreadExemplo t1 = new ThreadExemplo(1000, '-');
        ThreadExemplo t2 = new ThreadExemplo(1000, '>');
        ThreadExemplo t3 = new ThreadExemplo(1000, '<');
        ThreadExemplo t4 = new ThreadExemplo(1000, 'o');
        
        t1.setPriority(10);
        t2.setPriority(1);
        t3.setPriority(1);
        t4.setPriority(2);
        
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        
        
    }
    
}
