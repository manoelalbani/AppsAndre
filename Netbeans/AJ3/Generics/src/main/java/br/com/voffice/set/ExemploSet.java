/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.set;
import br.com.voffice.set.Pessoa;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;


/**
 *
 * @author manoa
 */
public class ExemploSet {
    
                public static void main(String[] args) {
                    Pessoa p1 = new Pessoa("Maria", "11111");
                Pessoa p2 = new Pessoa("joao", "22222");
                Pessoa p3 = new Pessoa("ana", "333333");
                Pessoa p4 = new Pessoa("Maria", "11111");
              
                Set pessoas = new HashSet();
                pessoas.add(p1);
                pessoas.add(p2);
                pessoas.add(p3);
                pessoas.add(p4);
                Iterator i = pessoas.iterator();
                for (Object o:pessoas){
                    System.out.println(((Pessoa)o).getNome());
                }
                System.out.println("while");
                while(i.hasNext()){
                    
                    System.out.println(((Pessoa)i.next()).getNome());
                }
    }
                
            
}
