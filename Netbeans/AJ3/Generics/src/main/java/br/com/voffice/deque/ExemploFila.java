package br.com.voffice.deque;
import java.util.Deque;
import java.util.LinkedList;

import br.com.voffice.treeset.comparator.Pessoa;

public class ExemploFila {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque fila = new LinkedList();
		System.out.println("Entrando na fila p1" );
		fila.add(new Pessoa("Maria", "111"));
		System.out.println("Entrando na fila p2" );
		fila.add(new Pessoa("Samara", "222"));
		System.out.println("Entrando na fila p3" );
		fila.add(new Pessoa("Joaquim", "333"));
		System.out.println("Entrando na fila p4" );
		fila.add(new Pessoa("Pedro", "444"));
		System.out.println("Entrando na fila p5" );
		fila.add(new Pessoa("Marta", "555"));
		
		while (!fila.isEmpty()){
			System.out.println(fila.pollFirst());
		}
		
	}

}
