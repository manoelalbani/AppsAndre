/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.treeset.ordenacao;

import java.util.Arrays;


/**
 *
 * @author manoa
 */
public class ExemploOrdenacao {
              public static void main(String[] args) {
                Pessoa p1 = new Pessoa("loa", "20202020");
        		Pessoa p2 = new Pessoa("arthur", "2222");
        		Pessoa p3 = new Pessoa("beatriz", "3333");
        		Pessoa p4 = new Pessoa("andre", "33444");
        		Pessoa p5 = new Pessoa("jana", "3434344");
        		Pessoa p6 = new Pessoa("uol", "333544");
                
        		Pessoa[] pessoas = {p1,p2,p3,p4,p5,p6};
        		
        		 for(Pessoa p:pessoas){
                     System.out.printf("Nome: %s cpf: %s %n", p.getNome(), p.getCpf());
                 }
        		System.out.println("++++++++++++");
                
                Arrays.sort(pessoas);
                
                 for(Pessoa p:pessoas){
                  System.out.printf("Nome: %s cpf: %s %n", p.getNome(), p.getCpf());
              }
    }
}
