/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.treeset.comparator;

import java.util.Comparator;

/**
 *
 * @author manoa
 */
public class ComparadorCpf implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        return ((Pessoa)o1).getCpf().compareTo(((Pessoa)o2).getCpf());
    }

    
    
}
