/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.treeset.comparator;

/**
 *
 * @author manoa
 */
public class Pessoa {
        private String nome;
	private String cpf;

	public Pessoa(String nome, String cpf){
		this.nome=nome;
		this.cpf=cpf;
	}

	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return nome;
	}
	public void setCpf(String cpf){
		this.cpf=cpf;
	}
	public String getCpf(){
		return cpf;
	}

    @Override
    public int hashCode() {
        return this.nome.hashCode()+this.cpf.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
         Pessoa other = (Pessoa)obj;
         return obj !=null&&this.nome.equals(other.nome)&&this.cpf.equals(other.cpf);
    }

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", cpf=" + cpf + "]";
	}
    
    
    
    
}
