/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.map;

import java.util.NavigableMap;
import java.util.TreeMap;

/**
 *
 * @author manoa
 */
public class ExemploNavigableMap {
    public static void main(String[] args) {
        Integer[] chaves = {1, 5, 7,4,9,6,10,8,2,3};
        
        NavigableMap mapa = new TreeMap();
        for(Integer i : chaves){
            mapa.put(i, "elemento "+i);
        }
        
        System.out.println("map.lowerKey(6) = "+ mapa.lowerKey(6));
        
        NavigableMap sub = mapa.subMap(2, true, 5, true);
        
       for(Object e: sub.values()){
           System.out.println(e);
       }
        
        
    }
}
