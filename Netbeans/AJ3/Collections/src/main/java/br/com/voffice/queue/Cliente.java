package br.com.voffice.queue;

public class Cliente {
	private String nome = null;
	private int id;
	private int qtdPedidos;
	public Cliente(int id,String nome, int qtdPedidos) {
		super();
		this.nome = nome;
		this.id = id;
		this.qtdPedidos = qtdPedidos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQtdPedidos() {
		return qtdPedidos;
	}
	public void setQtdPedidos(int qtdPedidos) {
		this.qtdPedidos = qtdPedidos;
	}
	@Override
	public String toString() {
		return "Cliente [nome=" + nome + ", id=" + id + ", qtdPedidos=" + qtdPedidos + "]";
	}
	
	
}
