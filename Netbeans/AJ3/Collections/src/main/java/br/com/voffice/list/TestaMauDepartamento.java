package br.com.voffice.list;


public class TestaMauDepartamento{
	public static void main(String[] args){
		MauDepartamento dep = new MauDepartamento();
		Pessoa[] pessoas = new Pessoa[2];
		Pessoa p1 = new Pessoa();
		p1.inicializaPessoa("Maria", "111111");
		pessoas[0]=p1;
		Pessoa p2 = new Pessoa();
		p2.inicializaPessoa("Joaquim", "2222222");
		pessoas[1]=p2;
		
		dep.setNome("Departamento de Tecnologia");
		dep.setPessoas(pessoas);
		dep.imprimeDados();

		//aumentando o array original que era de 2 agora vai para 2+1
		Pessoa[] original = dep.getPessoas();
		Pessoa[] novoArray = new Pessoa[original.length+1];
		for (int i=0; i<original.length;i++){
			novoArray[i]=original[i];
		}
		//colocando mais uma pessoa no array
		Pessoa p3 = new Pessoa();
		p3.inicializaPessoa("Amanda", "88888888");
		novoArray[original.length]=p3;
		original=novoArray;
		//colocando o array novo no departamento.
		dep.setPessoas(original);
		dep.imprimeDados();
	}
}