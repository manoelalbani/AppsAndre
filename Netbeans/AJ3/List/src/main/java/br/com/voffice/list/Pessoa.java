/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.list;

/**
 *
 * @author manoa
 */
public class Pessoa {
    private String nome;
	private String cpf;

	public void inicializaPessoa(String nome, String cpf){
		setNome(nome);
		setCpf(cpf);
	}

	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return nome;
	}
	public void setCpf(String cpf){
		this.cpf=cpf;
	}
	public String getCpf(){
		return cpf;
	}

    @Override
    public int hashCode() {
        return this.nome.hashCode()+this.cpf.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
         Pessoa other = (Pessoa)obj;
         return obj !=null&&this.nome.equals(other.nome)&&this.cpf.equals(other.cpf);
    }
}
