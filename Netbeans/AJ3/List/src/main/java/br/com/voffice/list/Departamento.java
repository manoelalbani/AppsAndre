/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.list;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manoa
 */
public class Departamento {
    private String nome;
    private List pessoas = new ArrayList();

	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return nome;
	}
	public void setPessoas(List pessoas){
		this.pessoas=pessoas;
	}
	public List getPessoas(){
		return pessoas;
	}
	public void addPessoa(Pessoa p){
            //com list
            this.pessoas.add(p);
                //com array
                //int tamanho=pessoas ==null ? 0 :pessoas.length;
		//Pessoa[] temp = new Pessoa[tamanho+1];
		//for (int i=0;i<tamanho;i++){
		//	temp[i]=pessoas[i];
		//} 
		//temp[tamanho]=p;
		//pessoas=temp;
	}
	public void imprimeDados(){
		System.out.println("=======================");
		System.out.println("Dados do Departamento: ");
		System.out.println("Nome: "+nome);
		System.out.println("=======================");
		System.out.println("Funcionarios");
		for(Object o:pessoas){
			System.out.println("Nome: "+((Pessoa)o).getNome());
			System.out.println("CPF: "+((Pessoa)o).getCpf());
			System.out.println("=======================");
		}
		
		
	}
}
