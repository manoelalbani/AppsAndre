
package ExpressaoRegular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author manoa
 */
public class ValidacaoComRegex {
    public static void main(String[] args) {
        String param = "004.242.109-83";
        if(validaEmaill(param)){
            System.out.println(param+ " eh um email valido");
        }else {
            System.out.println(param+" nao eh um email valido");
        }
        if(validaFormatoCpf(param)){
            System.out.println(param+ " eh um cpf valido");
        }else {
            System.out.println(param+" nao eh um cpf valido");
        }
        
      
    }
     private static boolean validaEmaill(String email){
            return Pattern.matches("[A-Za-z0-9._-]+@([A-Za-z]+\\.)+[A-Za-z]+", email);
        }
     public static boolean  validaFormatoCpf(String cpf){
            Pattern padraoCPF = Pattern.compile("\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}");
            Matcher matcher = padraoCPF.matcher(cpf);
            return matcher.matches();
        }
}
