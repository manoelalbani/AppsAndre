/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExpressaoRegular;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author manoa
 */
public class BuscaComRegex {
    public static void main(String[] args) {
        String texto = "O 2o colocado chegou as 11h26m33s";
        //aqui colocamos a expressao regular
        Pattern padrao = Pattern.compile("\\s\\d\\w");
        //aqui fazemso a busca da expressao junto ao texto que estamos buscando
        Matcher mathes = padrao.matcher(texto);
        while(mathes.find()){
            System.out.println("Encontrei o valor '"+mathes.group()+"'na posicao"+mathes.start());
        }
    }
    
}
