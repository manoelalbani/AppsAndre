/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.voffice.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author manoa
 */
public class FormatacaoDatas {
    public static void main(String[] args) {
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatador2 = new SimpleDateFormat("dd/MMMM/yyyy");
        SimpleDateFormat formatador3 = new SimpleDateFormat("EEEE, dd/MMMM/yyyy");
        SimpleDateFormat formatador4 = new SimpleDateFormat("EEEE, dd 'de' MMMM 'de' yyyy");
        Date data = new Date();
        System.out.println("Data - "+formatador.format(data));
        System.out.println("Data - "+formatador2.format(data));
        System.out.println("Data - "+formatador3.format(data));
        System.out.println("Data - "+formatador4.format(data));
        
        try {
            Date data2 = formatador.parse("01/08/2016");
            System.out.println("data2 = "+data2);
        } catch (Exception e) {
            System.out.println("Nao consegui converter a data"+e.getMessage());
        }
        try {
            Date data3 = formatador.parse("01082016");
            System.out.println("data3 = "+data3);
        } catch (Exception e) {
            System.out.println("Nao consegui converter a data"+e.getMessage());
        }
        try {
            Date data4 = formatador4.parse("Quarta-feira, 20 de Abril de 2016");
            System.out.println("data4 = "+data4);
        } catch (Exception e) {
            System.out.println("Nao consegui converter a data"+e.getMessage());
        }
        
        //intercionalizacao
        
        System.err.println("-------------intercionalizacao-------------");
        
        Locale local = new Locale("en", "US");
        Locale local2 = new Locale("pt", "BR");
        Locale local3 = new Locale("fr", "FR");
        SimpleDateFormat sf1 = new SimpleDateFormat("EEEE MMMM dd yyyy", local);
        SimpleDateFormat sf2 = new SimpleDateFormat("EEEE MMMM dd yyyy", local2);
        SimpleDateFormat sf3 = new SimpleDateFormat("EEEE MMMM dd yyyy", local3);
        System.out.println("Data EN - "+ sf1.format(data));
        System.out.println("Data BR - "+ sf2.format(data));
        System.out.println("Data FR - "+ sf3.format(data));
        
        System.err.println("-------------dateFormat-------------");
        
        DateFormat df = DateFormat.getDateInstance();
        System.out.println("Data com dateFormat - "+df.format(new Date()));
        
        DateFormat df2 = DateFormat.getDateInstance(DateFormat.LONG);
        System.out.println("Data com dateFormat long - "+df2.format(new Date()));
        
        DateFormat df3 = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("en", "US"));
        System.out.println("Data com dateFormat Medium com local- "+df3.format(new Date()));
        
    }
}
