/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.voffice.data;

import java.util.Calendar;

/**
 *
 * @author manoa
 */
public class ExemploCalendario {
    
    public static void main(String[] args) {
        Calendar cal  = Calendar.getInstance();
        System.out.println(cal.getTime());
        System.out.println("ANO - "+cal.get(Calendar.YEAR));
        System.out.println("Mes - "+cal.get(Calendar.MONTH));
        System.out.println("Dia - "+cal.get(Calendar.DATE));
        cal.set(Calendar.YEAR, 2017);
        
        System.out.println(cal.getTime());
        
        cal.add(Calendar.DAY_OF_MONTH, 40);
        
        
        System.out.println(cal.getTime());
        cal.roll(Calendar.DAY_OF_MONTH, 40);
        
        
        System.out.println(cal.getTime());
        
    }
    
}
