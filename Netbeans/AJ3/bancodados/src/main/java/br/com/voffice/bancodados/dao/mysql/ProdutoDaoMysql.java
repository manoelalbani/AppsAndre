/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados.dao.mysql;

import br.com.voffice.bancodados.dao.ProdutoDao;
import br.com.voffice.bancodados.model.Produto;
import br.com.voffice.bancodados.util.ConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manoa
 */
public class ProdutoDaoMysql implements ProdutoDao{

    @Override
    public Produto save(Produto produto) {
        String sql;
        if (produto.getId()==null){
            sql = "INSERT INTO produtos(nome, codigo, preco, descricao, image) VALUES ('" 
                    +produto.getNome()+"','"+produto.getCodigo()+"', "+produto.getPreco()+", '"
                    +produto.getDescricao()+"', '"+produto.getImage()+"')";
                   
        }else{
            sql   = "UPDATE produtos SET "
                    +"nome = '"+produto.getNome()+"',"
                    +"codigo = '"+produto.getCodigo()+"',"
                    +"preco = "+produto.getPreco()+","
                    +"descricao = '"+produto.getDescricao()+"',"
                    +"image = '"+produto.getImage()+"'"
                    + " WHERE id = "+ produto.getId();
        }
        try {
            Connection conn = ConnectionManager.getConnection();
            Statement stmt = conn.createStatement();
            int ret = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            if(produto.getId()==null){
                ResultSet rsKey = stmt.getGeneratedKeys();
                if(rsKey.next()){
                    produto.setId(rsKey.getInt(1));
                }
                System.out.println("Produto Inserido");
            }else{
                 System.out.println("Produto alterado");
            }
            
           
        } catch (Exception e) {
            System.out.println("deu erro"+e);
        }
        return produto;
    }

    @Override
    public List findAll() {
       String sql = "SELECT * FROM produtos";
       List ret = new ArrayList();
       try ( Connection conn = ConnectionManager.getConnection();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
             ){
             while(rs.next()){
                 Produto p = new Produto(rs.getInt("id"), rs.getString("nome"), rs.getString("codigo"), 
                         rs.getDouble("preco"), rs.getString("descricao"), rs.getString("image"));
                 ret.add(p);
             }
            
            
        } catch (Exception e) {
            System.out.println("deu erro"+e);
        }
       return ret;
    }

    @Override
    public void remove(int id) {
        String sql = "DELETE from  produtos where id = ?";
        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                ){
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            System.out.println("produto "+id+" deletado");
        } catch (Exception e) {
            System.out.println("deu ruim"+e.getMessage());
        }
    }
    
    
}
