/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author manoa
 */
public class TestaConexaoJDBC {
    public static void main(String[] args) {
        try(Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/aj", "root", "")){
            
            System.out.println("Conectado");
        }catch(SQLException e){
            System.out.println("Não conectado"+ e.getMessage());
        }
    }
}
