/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados.util;

/**
 *
 * @author manoa
 */
import java.sql.Connection;
import java.sql.SQLException;


import java.sql.DriverManager;


public class ConnectionManager {

	/*
	 * Os valores das constantes abaixo devem estar de acordo com o banco de
	 * dados utilizado.
	 */
	private static final String DATABASE = "aj";
	private static final String IP = "localhost";
	private static final String STR_CON = "jdbc:mysql://" + IP + ":3306/" + DATABASE;
	private static final String USER = "aj";
	private static final String PASSWORD = "aj";

	public static Connection getConnection()  {
		Connection conn = null;
		try  {
			// a) Faca a abertura da conexao
		 conn = DriverManager.getConnection(STR_CON, USER,PASSWORD);
			System.out.println("[ConnectionManager]: Obtendo conexao");
		} catch (SQLException e) {
			System.out.println("erro "+ e);
		}
                return conn;
	}


        
}
