/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados.dao;
import br.com.voffice.bancodados.model.Produto;
import java.util.List;

/**
 *
 * @author manoa
 */
public interface ProdutoDao {
    public Produto save (Produto produto);
    public List findAll();
    public void remove(int id);
}
