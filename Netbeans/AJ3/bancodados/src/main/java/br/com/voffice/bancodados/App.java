/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados;

import br.com.voffice.bancodados.dao.mysql.ProdutoDaoMysql;
import br.com.voffice.bancodados.model.Produto;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author manoa
 */
public class App {
    public static void main(String[] args) {
        String menu = "Informe a opcao: \n"
                +"1 - cadastrar Produto. \n"
                +"2 - listar produto. \n"
                +"3 - Deletar produto. \n"
                +"S - Sair.";
        String opcao =JOptionPane.showInputDialog(menu);
        
        ProdutoDaoMysql dao = new ProdutoDaoMysql();
        while (!opcao.equalsIgnoreCase("s")) {
            switch(opcao){
                case "1":
                    String nome = JOptionPane.showInputDialog("Qual o nome do Produto?");
                    String codigo = JOptionPane.showInputDialog("Qual o codigo do Produto?");
                    Double preco = new Double(JOptionPane.showInputDialog("Qual o Preco do Produto?"));
                    String descricao = JOptionPane.showInputDialog("Qual Descricao do Produto?");
                    String image = JOptionPane.showInputDialog("Qual o imagem do Produto?");
                    
                    Produto p1 = new Produto(nome, codigo,preco ,descricao, image);
                    p1 = dao.save(p1);
                    JOptionPane.showMessageDialog(null, p1);
                    break;       
                case "2":
                    String txt = new String();
                    for(Object o: dao.findAll()){
                        txt += o.toString()+"\n";
                    }
                    JOptionPane.showMessageDialog(null, txt);
                    break;
                case "3":
                    String strId = JOptionPane.showInputDialog("Informe o codigo do produto ");
                    Integer id = new Integer(strId);
                    dao.remove(id);
                    break;    
                    
                    
            }
            opcao = JOptionPane.showInputDialog(menu);
        }
        
    }
}
