/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.bancodados.model;

/**
 *
 * @author manoa
 */
public class Produto {
    private Integer id;
    private String nome, codigo, descricao, image;
    private double preco;

    public Produto(Integer id, String nome,  String codigo,double preco, String descricao, String image) {
        this.id = id;
        this.nome = nome;
        this.codigo = codigo;
        this.descricao = descricao;
        this.image = image;
        this.preco = preco;
    }

    public Produto(String nome, String codigo , double preco,String descricao, String image) {
        this.nome = nome;
        this.codigo = codigo;
        this.descricao = descricao;
        this.image = image;
        this.preco = preco;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", codigo=" + codigo + ", descricao=" + descricao + ", image="
				+ image + ", preco=" + preco + "]";
	}
        

  
    
    
    
}
