/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.equals.modelo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author manoa
 */
public class PessoaTest {
    
  @Test
  public void testaEqualReflexao(){
      Pessoa x = new Pessoa(1, "Maria", "111111111");
      assertTrue(x.equals(x));
  }
  
  @Test
  public void testaSimetria(){
      Pessoa x = new Pessoa(1, "Maria da Silva", "1233131313");
      Pessoa y = new Pessoa(1, "Maria da Silva", "1233131313");
      assertTrue(x.equals(y)==y.equals(x));
  }
  @Test
  public void testaTransitividade(){
    Pessoa x = new Pessoa(1, "Maria da Silva", "1233131313");
    Pessoa y = new Pessoa(1, "Maria da Silva", "1233131313");
    Pessoa z = new Pessoa(1, "Maria da Silva", "1233131313");
    assertTrue( x.equals(y)&& y.equals(z) && x.equals(z));
        
  }
  @Test
  public void testaConistencia(){
    Pessoa x = new Pessoa(1, "Maria da Silva", "1233131313");
    Pessoa y = new Pessoa(1, "Maria da Silva", "1233131313");
      assertTrue(x.equals(y)&&x.equals(y)&&x.equals(y));
  } 
  @Test
  public void testaNull(){
    Pessoa x = new Pessoa(1, "Maria da Silva", "1233131313");
      assertFalse(x.equals(null));
  }
  @Test
  public void testaHashCode(){
    Pessoa x = new Pessoa(1, "Maria da Silva", "1233131313");
    Pessoa y = new Pessoa(1, "Maria da Silva", "1233131313");
      assertTrue(x.equals(y)&&x.hashCode()==y.hashCode());
  }

    
}
