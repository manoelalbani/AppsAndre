package br.com.voffice.generics;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import br.com.voffice.set.Pessoa;

public class ExemploGenerics {
	public static void main(String[] args) {
		Pessoa p1 = new Pessoa("Maria", "11111");
        Pessoa p2 = new Pessoa("joao", "22222");
        Pessoa p3 = new Pessoa("ana", "333333");
        Pessoa p4 = new Pessoa("Maria", "11111");
      // aqui eu faço a config de generic, 
        // nao é necessario realizar cast, 
        // é só fazer a config na collection e depois ele ja 
        //ve que eh daquele tipo.
        List<Pessoa> pessoas = new ArrayList();
        pessoas.add(p1);
        pessoas.add(p2);
        pessoas.add(p3);
        pessoas.add(p4);
        
        for(Pessoa p:pessoas){
        	System.out.println(p);
        }
     }
}
