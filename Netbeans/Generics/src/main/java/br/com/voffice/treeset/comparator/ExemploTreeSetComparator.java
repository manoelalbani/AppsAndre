/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.treeset.comparator;

import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author manoa
 */
public class ExemploTreeSetComparator {
                public static void main(String[] args) {
                Pessoa c1 = new Pessoa("zebra", "1111");
		Pessoa c2 = new Pessoa("arthur", "2222");
		Pessoa c3 = new Pessoa("beatriz", "3333");
		Pessoa c4 = new Pessoa("andre", "33444");
                Pessoa c5 = new Pessoa("jana", "3434344");
                Pessoa c6 = new Pessoa("uol", "333544");
                
		
                NavigableSet<Pessoa> pessoas = new TreeSet(new ComparadorCpf());
                
                pessoas.add(c1);
		pessoas.add(c2);
		pessoas.add(c3);
		pessoas.add(c4);
                pessoas.add(c5);
                pessoas.add(c6);
                
                for(Pessoa o: pessoas){
                    System.out.println(o.getNome()+" cpf "+o.getCpf());
                }
    }
        
}
