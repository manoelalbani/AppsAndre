package br.com.voffice.list;


public class TestaDepartamento{
	public static void main(String[] args){
		Departamento dep = new Departamento();
		
		Pessoa p1 = new Pessoa();
		p1.inicializaPessoa("Maria", "111111");
		//adicionando pessoa

		dep.addPessoa(p1);
		Pessoa p2 = new Pessoa();
		p2.inicializaPessoa("Joaquim", "2222222");
		//adicionando pessoa
		dep.addPessoa(p2);
		dep.setNome("Departamento de Tecnologia");
		
		dep.imprimeDados();

		//colocando mais uma pessoa no array
		Pessoa p3 = new Pessoa();
		p3.inicializaPessoa("Amanda", "88888888");
		dep.addPessoa(p3);
		dep.imprimeDados();
	}

}