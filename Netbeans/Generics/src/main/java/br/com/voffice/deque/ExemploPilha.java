package br.com.voffice.deque;

import java.util.Deque;
import java.util.LinkedList;

import br.com.voffice.treeset.comparator.Pessoa;

public class ExemploPilha {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque pilha = new LinkedList();
		System.out.println("Entrando na pilha p1" );
		pilha.add(new Pessoa("Maria", "111"));
		System.out.println("Entrando na pilha p2" );
		pilha.add(new Pessoa("Samara", "222"));
		System.out.println("Entrando na pilha p3" );
		pilha.add(new Pessoa("Joaquim", "333"));
		System.out.println("Entrando na pilha p4" );
		pilha.add(new Pessoa("Pedro", "444"));
		System.out.println("Entrando na pilha p5" );
		pilha.add(new Pessoa("Marta", "555"));
		
		while(!pilha.isEmpty()){
			System.out.println(pilha.pollLast());
		}
	}

}
