/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.treeset.comparator;

import java.util.Comparator;

/**
 *
 * @author manoa
 */
public class ComparadorCpf implements Comparator<Pessoa>{

    @Override
    public int compare(Pessoa o1, Pessoa o2) {
        return o1.getCpf().compareTo(o2.getCpf());
    }

    
    
}
