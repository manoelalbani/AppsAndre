/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.equals.modelo;

/**
 *
 * @author manoa
 */
public class Pessoa {
    private Integer id;
    private String nome;
    private String cpf;

    public Pessoa(Integer id, String nome, String cpf){
        this.id=id;
        this.nome=nome;
        this.cpf=cpf;
    }
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public boolean equals(Object obj) {
       return obj!=null && this.id == ((Pessoa)obj).id;
    }

    @Override
    public int hashCode() {
        return this.nome.hashCode()+this.id+this.cpf.hashCode();
    }
    
    
    
}
