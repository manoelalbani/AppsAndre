/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.queue;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author manoa
 */
public class TestePriorityQueue {
    public static void main(String[] args) {
        
    	ComparadorClienteQtdPedidos comp = new ComparadorClienteQtdPedidos();
        
    	Queue<Cliente> clientes = new PriorityQueue(11, comp);
    	
    	clientes.add(new Cliente(1, "Cliente 01", 205));
    	clientes.add(new Cliente(2, "Globalcode", 43));
    	clientes.add(new Cliente(3, "Cliente 02", 89));
    	clientes.add(new Cliente(1, "Cliente 03", 20));
    	while (!clientes.isEmpty()){
    		System.out.println("Processando cliente: ");
    		System.out.println(clientes.poll());
    	}
    }

	
}
