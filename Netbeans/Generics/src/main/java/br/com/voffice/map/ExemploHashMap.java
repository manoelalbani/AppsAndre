/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import br.com.voffice.treeset.comparator.Pessoa;
/**
 *
 * @author manoa
 */
public class ExemploHashMap {
    public static void main(String[] args) {
    	//aqui configura o generic da chave e do objeto 
    	// q  ta sendo colocado.
    	Map<String, Pessoa> mapa = new HashMap<>();
		
    	Pessoa p1  = new Pessoa("Maria", "111");
		
		Pessoa p2  = new Pessoa("Samara", "222");
		
		Pessoa p3  = new Pessoa("Joaquim", "333");
		
		Pessoa p4  = new Pessoa("Pedro", "444");
		
		Pessoa p5  = new Pessoa("Marta", "555");
		
		mapa.put(p1.getCpf(), p1);
		mapa.put(p2.getCpf(), p2);
		mapa.put(p3.getCpf(), p3);
		mapa.put(p4.getCpf(), p4);
		mapa.put(p5.getCpf(), p5);
		
		Set<String> chaves = mapa.keySet();
		for (String c :chaves){
			System.out.println(mapa.get(c));
		}
		System.out.println("=========");
		Collection<Pessoa> valores = mapa.values();
		for (Pessoa v: valores){
			System.out.println(v);
		}
		System.out.println("=========");
		
		System.out.println(mapa.get("444"));
		
		System.out.println("=========");
		
		System.out.println(mapa.get(444));
	}
}
