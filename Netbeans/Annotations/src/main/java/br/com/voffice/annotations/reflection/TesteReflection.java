/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.annotations.reflection;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author treinamento
 */
public class TesteReflection {
    public static void main(String[] args) {
        try {
            Class clazz = Class.forName("br.com.voffice.annotations.model.Pessoa");
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                System.out.println(f.getName() + " = " + f.getType().getName());
            }
            
            Object o = clazz.newInstance(); // = new Pessoa()
            
            System.out.println(o.getClass().getName() + " = " + o);
            
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TesteReflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(TesteReflection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(TesteReflection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
