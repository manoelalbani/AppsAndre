/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.annotations.gui;

import br.com.voffice.annotations.GuiVisible;
import java.lang.reflect.Field;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author treinamento
 */
public class GUIGenerator extends JPanel {
    public static final int STEP_H = 20;
    private String className;
    private int lastWidth = 10;
    private int lastHeight = 10;

    public GUIGenerator() {
        this.setLayout(null);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
        loadClass();
    }
    
    public void loadClass() {
        Class classe = null;
        try {
            classe = Class.forName(className);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        
        Field[] fields = classe.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(GuiVisible.class)) {
                buildGUIField(field);
            }
        }
                
    }
    
    public void buildGUIField(Field field) {
        GuiVisible anotacao = field.getAnnotation(GuiVisible.class);
        String nomeCampo = anotacao.value();
        
        if (nomeCampo.equals("")) {
            nomeCampo = field.getName();
        }
        
        String textoAjuda = anotacao.textoAjuda();
        
        JLabel label = new JLabel(nomeCampo);
        
        JTextField txCampo = new JTextField();
        txCampo.setText(textoAjuda);
        
        label.setBounds(lastWidth, lastHeight, 200, 15);
        lastHeight += STEP_H;
        txCampo.setBounds(lastWidth, lastHeight, 200, 15);
        lastHeight += STEP_H;
        
        this.add(label);
        this.add(txCampo);
        
    }
    
    public void bind(Object entidade) {
        if (getClassName() == null) {
            setClassName(entidade.getClass().getName());
        }
    }
    
}
