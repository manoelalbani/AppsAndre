/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.annotations.teste;

import br.com.voffice.annotations.gui.GUIGenerator;
import br.com.voffice.annotations.model.Pessoa;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author treinamento
 */
public class TesteGUIGenerator {
    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI Generator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        GUIGenerator generator = new GUIGenerator();
        Pessoa pessoa = new Pessoa();
        generator.bind(pessoa);
        
        frame.add(generator);
        
        frame.setPreferredSize(new Dimension(300, 300));
        frame.pack();
        frame.setVisible(true);
        
    }
}
