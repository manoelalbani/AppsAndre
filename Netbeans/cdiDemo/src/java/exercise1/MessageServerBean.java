package exercise1;

import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author nbuser
 */
@Named
@Stateless
public class MessageServerBean {

    public String getMessage() {
        return "Hello Weld!";
    }
}
