/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.swing.combo;

import br.com.voffice.exemplodesktop.swing.model.Banco;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author manoa
 */
public class ExemploCombo extends JFrame{
    List<Banco> bancos;
    public ExemploCombo (String titulo){
        setTitle(titulo);
       
        bancos= new ArrayList<>();
        bancos.add(new Banco(1, "Banco 001"));
        bancos.add(new Banco(2, "Banco 002"));
        bancos.add(new Banco(3, "Banco 003"));
        bancos.add(new Banco(4, "Banco 004"));
        JComboBox combo = new JComboBox(new Vector(bancos));
        add(combo);
         setSize(1000,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
    public static void main(String[] args) {
        ExemploCombo ex = new ExemploCombo("aaa");
    }
    
}
