/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.evento.classeexterna;

import br.com.voffice.exemplodesktop.evento.EventosMouseListener;
import br.com.voffice.exemplodesktop.evento.LimparMouseListener;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author manoa
 */
public class Frame extends JFrame{
    private JTextField txt;
    public Frame(){
        setTitle("TratamentoBotao");
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        txt=new JTextField("sdsdsdsd");
        painel.add(txt);
        JButton btnSalvar = new JButton("Salvar");
        JButton btnLimpar = new JButton("Limpar");
        EventosMouseListener listener = new EventosMouseListener();
        LimparMouseListener limp = new LimparMouseListener(txt);
        btnLimpar.addMouseListener(limp);
        btnSalvar.addMouseListener(listener);
        painel.add(btnSalvar);
        painel.add(btnLimpar);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(painel);
        setSize(200,200);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Frame fr = new Frame();
    }
}
