/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.swing.layout;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.text.html.CSS;

/**
 *
 * @author manoa
 */
public class ExemploBordeLayout extends JFrame{

    public ExemploBordeLayout(String title)  {
        super(title);
        setLayout(new BorderLayout());
        setSize(500,150);
        add(montaPainelBotoes(), BorderLayout.SOUTH);
        add(new JLabel("login no sistema"),BorderLayout.NORTH);
        add(montapainelLogin(), BorderLayout.CENTER);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
        
    }
    public static void main(String[] args) {
        ExemploBordeLayout ex = new ExemploBordeLayout("login");
    }
    public JPanel montaPainelBotoes(){
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton btnSalvar = new JButton("Salvar");
        JButton btnExcluir = new JButton("Excluir");
        painel.add(btnSalvar);
        painel.add(btnExcluir);
        return painel;
    }
    
     public JPanel montapainelLogin(){
        JPanel jp = new JPanel(new GridLayout(2, 2));
        JLabel lUsername= new JLabel("UserName");
        JTextField txtUsername = new JTextField();
        JLabel lPassword = new JLabel("Password");
        JTextField txtPassword = new JPasswordField();
        jp.add(lUsername);
        jp.add(txtUsername);
        jp.add(lPassword);
        jp.add(txtPassword);
        return jp;
    }
    
    
}
