/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.swing.layout;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author manoa
 */
public class ExemploFlowLayout extends JFrame{

    public ExemploFlowLayout(String titulo) {
        setTitle(titulo);
        setSize(500,250);
        add(montaPainelBotoes());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
    public JPanel montaPainelBotoes(){
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton btnSalvar = new JButton("Salvar");
        JButton btnExcluir = new JButton("Excluir");
        painel.add(btnSalvar);
        painel.add(btnExcluir);
        return painel;
    }
    public static void main(String[] args) {
        ExemploFlowLayout ex = new ExemploFlowLayout("aaa");
    }
    
}
