/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.evento.proprioframe;

import br.com.voffice.exemplodesktop.evento.classeexterna.*;
import br.com.voffice.exemplodesktop.evento.EventosMouseListener;
import br.com.voffice.exemplodesktop.evento.LimparMouseListener;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author manoa
 */
public class Frame extends JFrame implements MouseListener{
    private JTextField txt;
    public Frame(){
        
        setTitle("TratamentoBotao");
        setLayout(new BorderLayout());
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        txt=new JTextField("sdsdsdsd");
        add(txt, BorderLayout.NORTH);
        JButton btnSalvar = new JButton("Salvar");
        JButton btnLimpar = new JButton("Limpar");
       
        btnLimpar.addMouseListener(this);
        btnSalvar.addMouseListener(this);
        painel.add(btnSalvar);
        painel.add(btnLimpar);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(painel);
        setSize(200,200);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Frame fr = new Frame();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
      txt.setText("");
    }

    @Override
    public void mousePressed(MouseEvent e) {
      
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      
    }

    @Override
    public void mouseEntered(MouseEvent e) {
      
    }

    @Override
    public void mouseExited(MouseEvent e) {
      
    }
}
