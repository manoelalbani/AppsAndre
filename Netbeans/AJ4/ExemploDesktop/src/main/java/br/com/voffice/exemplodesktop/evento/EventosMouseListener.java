/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.evento;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 *
 * @author manoa
 */
public class EventosMouseListener implements MouseListener{

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("MouseClicked");
    }

    @Override
    public void mousePressed(MouseEvent e) {
      System.out.println("MousePressed");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("MouseReleased");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("MouseEntered");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("mouseexited");
    }
    
}
