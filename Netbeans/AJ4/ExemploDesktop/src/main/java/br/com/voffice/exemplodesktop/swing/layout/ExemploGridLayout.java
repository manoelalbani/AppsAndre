/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.swing.layout;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author manoa
 */
public class ExemploGridLayout extends JFrame{

    public ExemploGridLayout(String titulo) {
        setTitle(titulo);
        setSize(500,100);
        add(montapainelLogin());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
    
    public JPanel montapainelLogin(){
        JPanel jp = new JPanel(new GridLayout(2, 2));
        JLabel lUsername= new JLabel("UserName");
        JTextField txtUsername = new JTextField();
        JLabel lPassword = new JLabel("Password");
        JTextField txtPassword = new JPasswordField();
        jp.add(lUsername);
        jp.add(txtUsername);
        jp.add(lPassword);
        jp.add(txtPassword);
        return jp;
    }
    
    public static void main(String[] args) {
        ExemploGridLayout ex = new ExemploGridLayout("aiaa");
    }
    
}
