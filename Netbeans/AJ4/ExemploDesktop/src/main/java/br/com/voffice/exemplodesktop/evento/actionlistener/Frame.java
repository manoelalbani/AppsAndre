/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.evento.actionlistener;


import br.com.voffice.exemplodesktop.evento.classeanonima.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author manoa
 */
public class Frame extends JFrame  {
    private JTextField txt;
    public Frame(){
        
        setTitle("TratamentoBotao");
        setLayout(new BorderLayout());
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        txt=new JTextField("sdsdsdsd");
        add(txt, BorderLayout.NORTH);
        JButton btnSalvar = new JButton("Salvar");
        btnSalvar.setMnemonic('S');
         //exemplo de classe anonima instaciando diretamente sem atribuir a uma variavel.
        /*btnSalvar.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
              System.out.println("Salvando");
            }
            
        });
        */
        //criando uma classe anonima de action listiner, que vai aceitar tanto comando de clic qnt
        //por teclado
        btnSalvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("botao ativando"+e.getActionCommand());
                txt.setText("botao ativando"+e.getActionCommand());
            }
        });
        JButton btnLimpar = new JButton("Limpar");
        /*exemplo de classe anonima, atribuindo a uma variavel de classe
        MouseListener limpar = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                txt.setText("");
            }
            
        };*/
       btnLimpar.setMnemonic('L');
        btnLimpar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txt.setText("");
            }
        });
       
        painel.add(btnSalvar);
        painel.add(btnLimpar);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(painel);
        setSize(200,200);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Frame fr = new Frame();
    }

  class LimparListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
           
        }
        
    }
  class SalvarListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
          
        }
        
    }
}
