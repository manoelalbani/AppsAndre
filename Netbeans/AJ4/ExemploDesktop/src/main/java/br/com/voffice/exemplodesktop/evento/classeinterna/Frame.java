/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.voffice.exemplodesktop.evento.classeinterna;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author manoa
 */
public class Frame extends JFrame  {
    private JTextField txt;
    public Frame(){
        
        setTitle("TratamentoBotao");
        setLayout(new BorderLayout());
        JPanel painel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        txt=new JTextField("sdsdsdsd");
        add(txt, BorderLayout.NORTH);
        JButton btnSalvar = new JButton("Salvar");
        JButton btnLimpar = new JButton("Limpar");
       
        btnLimpar.addMouseListener(new LimparListener());
        btnSalvar.addMouseListener(new SalvarListener());
        painel.add(btnSalvar);
        painel.add(btnLimpar);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(painel);
        setSize(200,200);
        setVisible(true);
    }
    
    public static void main(String[] args) {
        Frame fr = new Frame();
    }

  class LimparListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
           txt.setText("");
        }
        
    }
  class SalvarListener extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
            System.out.println("Salvando");
        }
        
    }
}
